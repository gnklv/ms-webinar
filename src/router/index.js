import Vue from 'vue';
import Router from 'vue-router';
import Form from '@/components/Form';
import List from '@/components/List';
import Users from '@/components/Users';
import Types from '@/components/Types';
import Banners from '@/components/Banners';

Vue.use(Router);

export default new Router({
	mode: 'history',
  routes: [
    {
    	path: '/list',
    	name: 'List',
    	component: List
    },
    {
      path: '/',
      name: 'Form',
      component: Form
    },
    {
      path: '/users',
      name: 'Users',
      component: Users
    },
    {
      path: '/types',
      name: 'Types',
      component: Types
    },
    {
      path: '/banners',
      name: 'Banners',
      component: Banners,
      children: [
        {
          path: ':id',
          component: Banners
        }
      ]
    }
  ]
})
