// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import VueResource from 'vue-resource';
import VueMoment from 'vue-moment';
import VueMask from 'v-mask';
import lodash from 'lodash';

Vue.use(VueResource);
Vue.use(VueMoment);
Vue.use(VueMask);

Vue.prototype._ = lodash;

Vue.http.options.xhr = {withCredentials: true};
Vue.http.options.credentials = true;
Vue.http.options.emulateJSON = true;

Vue.config.productionTip = false;


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App },
  http: {
  	root: 'http://localhost:8464'
  }
});